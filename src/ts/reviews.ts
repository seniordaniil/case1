import { Component, Vue } from "vue-property-decorator";
import moment from "moment";
moment.locale("ru");

const LIMIT = 3;

interface IReviews {
    id: number | string;
    name: string;
    lastGame: Date;
    date: Date;
    content: string;
    rank: number;
}

@Component
export default class Reviews extends Vue {
    private paginate: number = LIMIT;

    get isMore(): boolean {
        return this.$store.state.reviews.length > this.paginate;
    }

    loadMore() {
        let count = this.$store.state.reviews.length - this.paginate;
        if (count >= LIMIT) this.paginate += LIMIT;
        else this.paginate += count;
    }

    public get reviews() {
        return (this.$store.state.reviews as IReviews[])
        .sort((a, b) => b.date.getTime() - a.date.getTime())
        .slice(0, this.paginate)
        .map(review => ({
            ...review,
            date: moment(review.date).format("Do MMMM"),
            lastGame: moment(review.lastGame).format("Do MMMM")
        }));
    }
}