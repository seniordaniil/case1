import { Vue, Component } from "vue-property-decorator";
import { mapGetters } from "vuex";
import Reviews from "../components/Reviews.vue";

@Component({
    computed: {...mapGetters(["rating"])},
    components: { reviews: Reviews }
})
export default class Review extends Vue {
    public get reviews_count() {
        return this.$store.state.reviews.length;
    }
}