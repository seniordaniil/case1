import { Vue, Component } from "vue-property-decorator";

@Component
export default class AddReview extends Vue {
    public srank: number = 5;
    public name: string = "";
    public review: string = "";

    public send() {
        if (!this.name || !this.review ) return;
        const date = new Date();
        const id = (this.$store.state.reviews as []).reduce((accum, {id: current}) => (current > accum ? current : accum), 0) + 1;
        this.$store.commit({
            type: "addReview",
            name: this.name,
            rank: this.srank,
            content: this.review,
            date,
            lastGame: date,
            id
        });
        this.$router.push({ name: "home" });
    }
}