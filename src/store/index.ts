import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        reviews: [{
            id: 1,
            name: "Alex",
            lastGame: new Date("2014-12-21T00:00:00.000Z"),
            date: new Date("2014-12-21T00:00:00.000Z"),
            content: `Очень клевая игра!`,
            rank: 5
        },
        {
            id: 2,
            name: "Micael",
            lastGame: new Date("2015-12-21T00:00:00.000Z"),
            date: new Date("2015-12-21T00:00:00.000Z"),
            content: `Я орал как маленькая девочка`,
            rank: 5
        },
        {
            id: 3,
            name: "Ирина",
            lastGame: new Date("2016-12-21T00:00:00.000Z"),
            date: new Date("2016-12-21T00:00:00.000Z"),
            content: `Кайф!`,
            rank: 5
        },
        {
            id: 4,
            name: "NoName",
            lastGame: new Date("2017-12-21T00:00:00.000Z"),
            date: new Date("2017-12-21T00:00:00.000Z"),
            content: `Еще один отзыв!`,
            rank: 4
        }]
    },
    getters: {
        reviews: state => state.reviews.sort((a, b) => (b.date.getTime() - a.date.getTime())),
        rating(state) {
            const ranks = [
                { rank: 5, count: 0 },
                { rank: 4, count: 0 },
                { rank: 3, count: 0 },
                { rank: 2, count: 0 },
                { rank: 1, count: 0 }
            ];
            let len = 0;
            for (let review of state.reviews) {
                const rank = ranks.find(rank => rank.rank === review.rank);
                if (rank) rank.count++;
                len++;
            }
            const p = len / 100;
            const r = ranks.map(rank => {
                let percent = rank.count / p;
                if (!isFinite(percent)) percent = 0;
                percent = Math.floor(percent);
                return {
                    rank: rank.rank,
                    percent
                };
            })
            .sort((a, b) => (b.rank - a.rank));
            const base = Math.pow(10, 1);
            let avg = ranks.reduce((accum, current) => (accum + (current.count * current.rank)), 0) / len;
            avg = (isFinite(avg)) ? avg : 0;
            avg = Math.round(avg * base) / base;
            return {
                avg,
                ranks: r
            };
        }
    },
    mutations: {
        addReview({ reviews }, payload) {
            reviews.push(payload);
        }
    }
});