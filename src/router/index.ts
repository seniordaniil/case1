import VueRouter from "vue-router";
import Vue from "vue";
import Review from "../components/Review.vue";
import AddReview from "../components/AddReview.vue";

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: "/", component: Review, name: "home" },
        { path: "/add", component: AddReview, name: "add-review" }
    ]
});

export default router;