import path from 'path';
import webpack from "webpack";
import { VueLoaderPlugin } from "vue-loader";
import HardSourceWebpackPlugin from "hard-source-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin";

const config: webpack.Configuration = {
    entry: "./src/index.ts",
    output: {
        filename: "[name].js",
        path: path.join(__dirname, "dist")
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
        plugins: [
            new TsconfigPathsPlugin({ configFile: path.join(__dirname, "src/tsconfig.json") })
        ]
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
                options: {
                    loader: {
                        ts: "ts-loader"
                    },
                    esModule: true,
                    exposeFilename: true
                }
            },
            {
                test: /\.[tj]sx?$/,
                loader: "ts-loader",
                options: {
                    configFile: path.join(__dirname, "src/tsconfig.json"),
                    appendTsSuffixTo: [/\.vue$/]
                }
            },
            {
                test: /\.css$/,
                use: [
                    "vue-style-loader",
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "vue-style-loader",
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
                loader: 'file-loader',
                options: {
                    outputPath: 'assets',
                },
            }
        ]
    },
    plugins: [
        new HardSourceWebpackPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            title: "App",
            template: path.join(__dirname, "src/index.ejs")
        })
    ],
    devServer: {
        host: "0.0.0.0",
        port: 8080,
        hotOnly: true,
        contentBase: path.join(__dirname, "dist"),
        compress: true
    }
};

export default config;